#!/bin/bash

if [[ "$OSTYPE" == "msys" ]]; then
   rm -rf ../vimfiles

   cp -rv ./vim/. ../

   cp .ideavimrc ~/_ideavimrc

fi

if [[ "$OSTYPE" == "linux-gnu" ]]; then
   rm -rf ~/.vim
   mkdir ~/.vim
   cp -rv ./vim/vimfiles/* ~/.vim/
   cp -rv ./vim/.vimrc ~/.vimrc
   cp ./.ideavimrc ~/.ideavimrc
fi


