function! GetGitBranchName() 
   let l:name = gitbranch#name()
   return strlen(l:name) > 0 ? '('. l:name .')' : ''
endfunction

""""""""""""""""""""""""""""""""""


if has("win32")
  "Windows options here
   set clipboard=unnamed

   set backupdir-=.
   set backupdir^=$TEMP
else
   "Linux
   set clipboard=unnamedplus
   set directory=$HOME/.vim/swapfiles//
endif

""""""""""""""""""""""""""""""""""

set encoding=utf-8

filetype plugin indent on

set tabstop=3
set shiftwidth=3
set expandtab

set number "line numbers

set backspace=indent,eol,start

set ignorecase
set smartcase

set virtualedit=block

set spl=pl,en
set spell
set hlsearch
set nowrap

vnoremap // y/\V<C-R>=escape(@",'/\')<CR><CR> " search selected text with //

noremap <C-l> :Autoformat<CR> "Ctrl-L"

" move through wrapped lines:
noremap <silent> k gk
noremap <silent> j gj
"noremap <silent> 0 g0
"noremap <silent> $ g$

if has('gui_running')
   syntax enable
   set background=dark
   colorscheme solarized

   let g:auto_save = 1 "enable autosave only in gui mode

   "statusline:
   set laststatus=2

   set statusline=%1*\ %* "first space

   set statusline+=%2* "color start
   set statusline+=%f%r
   set statusline+=\ %{GetGitBranchName()}

   set statusline+=%= "right side

   set statusline+=%l:%c "line:column
   set statusline+=\ 
   set statusline+=\ %y "filetype e.g. [java]
   set statusline+=\ 
   set statusline+=\ %{&fileencoding?&fileencoding:&encoding} "encoding
   set statusline+=\ 
   set statusline+=\[%{&fileformat}\] "format
   set statusline+=\ 
   set statusline+=0x%-02B "character value
   set statusline+=%* "color end

   set statusline+=%1*\ %* " last space

   hi User1 guibg=#000000 guifg=#000000 gui=NONE "bold, underline, reverse, italic, none
   hi User2 guibg=#000000 guifg=#40DDDD gui=NONE
   "
endif

